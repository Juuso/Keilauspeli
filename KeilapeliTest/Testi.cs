﻿using Keilapeli;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeilapeliTest

 /* 
  Lista testeistä:
  - Kaikki kouruun
  - Aina yksi piste
  - Paikko
  - Kaato
  - Kierroksesta täydet pisteet
    
 */

{
    [TestFixture]
    public class Testi
    { 
        [Test]
    public void KaikkiKouruun()
        {
            var peli = new Keilaus();
            Heitto(0, 20);
            Assert.AreEqual(0, peli.Tulos);
        }

        private void Heitto(int v1, int v2)
        {
            
        }

        [Test]

        public void AinaYksiPiste()
        {
            var peli = new Keilaus();

            for (var i = 0; i < 20; i++)
                Heitto(1, 20);
            Assert.AreEqual(20, peli.Tulos);            
            }



        [Test]

        public void Täyskaato()
        {
            var peli = new Keilaus();
            
                peli.Heitto(20);
            Assert.AreEqual(20, peli.Tulos);
        }

        [Test]

        public void Paikko()
        {
            var peli = new Keilaus();
            
            peli.Heitto(5);
            peli.Heitto(5);
            peli.Heitto(5);
            
            Assert.AreEqual(15, peli.Tulos); 
        }

        [Test]

        public void KierroksestaTäydetPisteet()
        {
            var peli = new Keilaus();

            Heitto(10, 12);
            Assert.AreEqual(300, peli.Tulos);
        }



        }



      }
   
